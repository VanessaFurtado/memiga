package Modelo;

public class Exercicio {

	public int codigoExercicio;
	public int codgoMateria;
	public int nivel;
	public String Descricao;
	public String resposta;
	public String respostab;
	public String respostac;
	public String respostad;
	public int codigoConteudo;	
	
	
	public String getRespostab() {
		return respostab;
	}
	public void setRespostab(String respostab) {
		this.respostab = respostab;
	}
	public String getRespostac() {
		return respostac;
	}
	public void setRespostac(String respostac) {
		this.respostac = respostac;
	}
	public String getRespostad() {
		return respostad;
	}
	public void setRespostad(String respostad) {
		this.respostad = respostad;
	}
	public int getCodigoConteudo() {
		return codigoConteudo;
	}
	public void setCodigoConteudo(int codigoConteudo) {
		this.codigoConteudo = codigoConteudo;
	}
	public int getCodigoExercicio() {
		return codigoExercicio;
	}
	public void setCodigoExercicio(int codigoExercicio) {
		this.codigoExercicio = codigoExercicio;
	}
	public int getCodgoMateria() {
		return codgoMateria;
	}
	public void setCodgoMateria(int codgoMateria) {
		this.codgoMateria = codgoMateria;
	}
	public int getNivel() {
		return nivel;
	}
	public void setNivel(int nivel) {
		this.nivel = nivel;
	}
	public String getDescricao() {
		return Descricao;
	}
	public void setDescricao(String descricao) {
		Descricao = descricao;
	}
	public String getResposta() {
		return resposta;
	}
	public void setResposta(String resposta) {
		this.resposta = resposta;
	}

}
