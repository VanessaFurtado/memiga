package Modelo;

public class Conteudo {

	public String nomeConteudo;
	public String descricao;
	public int codigoMateria;
	public int codigoConteudo;
	
	public int getCodigoConteudo() {
		return codigoConteudo;
	}
	public void setCodigoConteudo(int codigoConteudo) {
		this.codigoConteudo = codigoConteudo;
	}
	public String getNomeConteudo() {
		return nomeConteudo;
	}
	public void setNomeConteudo(String nomeConteudo) {
		this.nomeConteudo = nomeConteudo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public int getCodigoMateria() {
		return codigoMateria;
	}
	public void setCodigoMateria(int codigoMateria) {
		this.codigoMateria = codigoMateria;
	}
	
	
	
}
