package Dao;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.JOptionPane;

import Modelo.Conteudo;
import Modelo.Exercicio;
import Modelo.Usuario;
import Visao.AprendaGUI;
import Visao.MainsGUI;
import Visao.OpcaoGUI;

public class DaoMemiga {

	// Configura essas vari‡veis de acordo com o seu banco
	private final String URL = "jdbc:mysql://localhost/memiga",
			USER = "root", 
			PASSWORD = "1234";

	private Connection con;
	private Statement comando;

	private void conectar() {
		try {
			con = ConFactory.conexao(URL, USER, PASSWORD, ConFactory.MYSQL);
			comando = con.createStatement();
			System.out.println("Conectado!");
		} catch (ClassNotFoundException e) {
			imprimeErro("Erro ao carregar o driver", e.getMessage());
		} catch (SQLException e) {
			imprimeErro("Erro ao conectar", e.getMessage());
		}
	}

	private void fechar() {
		try {
			comando.close();
			con.close();
			System.out.println("Conex�o Fechada");
		} catch (SQLException e) {
			imprimeErro("Erro ao fechar conex�o", e.getMessage());
		}
	}

	private void imprimeErro(String msg, String msgErro) {
		JOptionPane.showMessageDialog(null, msg, "Erro cr�tico", 0);
		System.err.println(msg);
		System.out.println(msgErro);
		System.exit(0);
	}

	public void insere(Usuario u){
		conectar();
		PreparedStatement insertUsuario = null;
		
		if(validaUsuario(u.getUsuario(), u.getSenha())==true){

		try {
			
			String sql = "INSERT INTO cadastro (nome, senha, email, nascimento, usuario) VALUES(?,?,?,?,?)";
			insertUsuario = con.prepareStatement(sql);
			insertUsuario.setString(1, u.getNome());
			insertUsuario.setString(2, u.getSenha());
			insertUsuario.setString(3, u.getEmail());
			insertUsuario.setDate(4, u.getNascimento());
			insertUsuario.setString(5, u.getUsuario());

			int r=insertUsuario.executeUpdate();

			if(r > 0){
				//comando.executeUpdate(sql);
				System.out.println("Inserida!");
				JOptionPane.showMessageDialog(null, "Cadastrado com sucesso!");	
				
				validaLogin(u.Usuario , u.senha);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Nome de usu�rio j� existente. Tente outro");
			imprimeErro("Erro ao inserir Usuario", e.getMessage());
		} finally {
			fechar();
		}
	}
	}
	
	 public void insereConteudo(Conteudo f){
			conectar();
			PreparedStatement insertFilme = null;

			try {
		
				String sql = "INSERT INTO conteudo (nomeConteudo,descricao,codigoMateria) VALUES(?,?,?)";
				insertFilme = con.prepareStatement(sql);
				insertFilme.setString(1, f.getNomeConteudo());
				insertFilme.setString(2, f.getDescricao());
				insertFilme.setInt(3, f.getCodigoMateria());

				int r=insertFilme.executeUpdate();

				if(r > 0){
					//comando.executeUpdate(sql);
					System.out.println("Inserida!");
				}
			} catch (SQLException e) {
				imprimeErro("Erro ao inserir Conteudo", e.getMessage());
			} finally {
				fechar();
			}
		}
	 
	public void altera(Usuario f){
		conectar();
		PreparedStatement alterarUsuario = null;
		
		try {
		
		String sql = "UPDATE CADASTRO " + "SET NOME=?,EMAIL=?,SENHA=?, NASCIMENTO=?" + "WHERE USUARIO = ? ";
		
		alterarUsuario = con.prepareStatement(sql);
		alterarUsuario = con.prepareStatement(sql);
		alterarUsuario.setString(1, f.getNome());
		alterarUsuario.setString(2, f.getSenha());
		alterarUsuario.setString(3, f.getEmail());
		alterarUsuario.setDate(4, f.getNascimento());
		
		int r=alterarUsuario.executeUpdate();

		if(r > 0){
			//comando.executeUpdate(sql);
			System.out.println("Alterado!");
		}
		
		}catch(SQLException e){
			imprimeErro("Erro ao alterar Usuario", e.getMessage());
		}
	     finally {
		    fechar();
	    }
	
	}
	
	public ArrayList <Usuario> listaTodos(){
		conectar();
		ArrayList <Usuario> fornecedor= new ArrayList<Usuario>();
		
		try
			{
			PreparedStatement selecionaUsuario = null;
			ResultSet rs = null;
			
			String sql = "SELECT * FROM USUARIO";
			
			selecionaUsuario = con.prepareStatement(sql);
			rs = selecionaUsuario.executeQuery(sql);
				
			while(rs.next())
				{
				Usuario f = new Usuario();
				f.setNome(rs.getString("Nome"));
				f.setEmail(rs.getString("Email"));
				f.setNascimento(rs.getDate(0));
				fornecedor.add(f);
				}
			
			}
			catch(SQLException e)
				{
				imprimeErro("Erro ao listar fornecedor",
				e.getMessage());
			
				}
			finally
			{
			fechar();
			}
		
		
		return fornecedor;
	}
	
	public ArrayList <Usuario> listaPorEmail(String text){
		conectar();
		ArrayList <Usuario> usuario= new ArrayList<Usuario>();
		
		try
			{
			PreparedStatement selecionaUsuario = null;
			ResultSet rs = null;
			
			String sql = "SELECT * FROM CADASTRO WHERE "+" EMAIL LIKE '"+ text + "%'";
			
			selecionaUsuario = con.prepareStatement(sql);
			rs = selecionaUsuario.executeQuery(sql);
				
			while(rs.next())
				{
				Usuario u = new Usuario();
				u.setNome(rs.getString("Nome"));
				u.setEmail(rs.getString("Email"));
				u.setNascimento(rs.getDate(0));
				usuario.add(u);
				}
			
			}
			catch(SQLException e)
				{
				imprimeErro("Erro ao listar fornecedor",
				e.getMessage());
			
				}
			finally
			{
			fechar();
			}
		
		
		return usuario;
	}
	
	public ArrayList <Exercicio> listaexercicio (int codigoConteudo){
		conectar();
		
		ArrayList <Exercicio> exercicio= new ArrayList<Exercicio>();
		
		try
			{
			PreparedStatement selecionaUsuario = null;
			ResultSet rs = null;
			
			String sql = "SELECT * FROM EXERCICIO WHERE CODIGOCONTEUDO = '"+ codigoConteudo +"'";
			
			selecionaUsuario = con.prepareStatement(sql);
			rs = selecionaUsuario.executeQuery(sql);
				
			while(rs.next())
				{
				Exercicio u = new Exercicio();
				u.setDescricao(rs.getString("descricao"));
				u.setResposta(rs.getString("resposta"));
				u.setRespostab(rs.getString("resposta2"));
				u.setRespostac(rs.getString("resposta3"));
				u.setRespostad(rs.getString("resposta4"));
				
				exercicio.add(u);
				
				}
			
			}
			catch(SQLException e)
				{
				imprimeErro("Erro ao listar fornecedor",
				e.getMessage());
			
				}
			finally
			{
			fechar();
			}
		
		
		return exercicio;
	}
		
	public boolean validaLogin(String usuario, String senha)
	{
	   try
	   {
		   conectar();
	       Statement stmt = con.createStatement();
	       String CHECK_USER = "SELECT * FROM cadastro WHERE usuario='"+usuario+"' AND senha='"+senha+"'";
	       ResultSet rs = stmt.executeQuery(CHECK_USER);
	       System.out.println("passou  select");

	       while(rs.next())
	        {
	    	   System.out.println("ntrou no while");
	            String user = usuario;
	            String pass = senha;

	            if(user.equals(rs.getString("usuario")))
	            {
	            	System.out.println("entrou no if1");
	                if(pass.equals(rs.getString("senha")))
	                {
	                    OpcaoGUI o = new OpcaoGUI();
	                    o.setVisible(true);
	                    return true;
	                }
	                else{ JOptionPane.showMessageDialog(null, "Senha inv�lida");
	                return false;
	            }
	            }
	            else {JOptionPane.showMessageDialog(null, "Usu�rio ou senha n�o existentes");
	            return false;
	            }}
	     fechar();
	     
	   }

	   catch(Exception er)
	   		{
		   System.out.println("entrou no catch");
	       JOptionPane.showMessageDialog(null, "Exception:\n" + er.toString());
	   		}
	   return false;
	}
	
	public boolean validaUsuario(String usuario, String email)
	{
	   try
	   {
	      conectar();
	       Statement stmt = con.createStatement();
	       String CHECK_USER = "SELECT * FROM cadastro WHERE usuario = '"+ usuario +"' AND email = '"+email+"'";
	       ResultSet rs = stmt.executeQuery(CHECK_USER);

	        while(rs.next())
	        {
	            String user = usuario;
	            String pass = email;

	            if((user.equals(rs.getString("usuario")) || (pass.equals(rs.getString("email")))))
	            {
	                	JOptionPane.showMessageDialog(null, "Usu�rio e email j� cadastrado");
	                	return false;
	                	}
	            }
	   }
	   catch(Exception er)
		   {
		       JOptionPane.showMessageDialog(null, "Exception:\n" + er.toString());
		   }
	   return true;
	}
	
	public ArrayList <Conteudo> listaPorNome(String nome){
		conectar();
		ArrayList <Conteudo> conteudo= new ArrayList<Conteudo>();
		
		try
			{
			
			Statement selecionaConteudo;
			selecionaConteudo = con.createStatement();
			String sql = "SELECT descricao FROM CONTEUDO WHERE nomeConteudo= '"+nome+ "'";
			selecionaConteudo = con.prepareStatement(sql);
			ResultSet rs = selecionaConteudo.executeQuery(sql);
				
			if(rs.next())
				{
				Conteudo c = new Conteudo();
				c.setDescricao(rs.getString("descricao"));
				System.out.println(rs.getString("descricao"));
				conteudo.add(c);
				}
			else{
				JOptionPane.showMessageDialog(null, "Conte�do n�o existente!");
			}
			}
			catch(SQLException e)
				{
				imprimeErro("Erro ao listar conteudo",
				e.getMessage());
				}
			finally
			{
			fechar();
			}
		return conteudo;
	}
	
	public ArrayList <Conteudo> listaPorMateria (int materia){
		conectar();
		ArrayList <Conteudo> conteudoMateria = new ArrayList<Conteudo>();
		
		try
			{
			PreparedStatement selecionaConteudo = null;
			ResultSet rs = null;
			
			String sql = "SELECT nomeConteudo, descricao, codigoConteudo FROM CONTEUDO WHERE codigoMateria = "+ materia;
			
			selecionaConteudo = con.prepareStatement(sql);
			rs = selecionaConteudo.executeQuery(sql);
				
			while(rs.next())
				{
				Conteudo c = new Conteudo();
				c.setNomeConteudo(rs.getString("nomeConteudo"));
				c.setDescricao(rs.getString("descricao"));
				c.setCodigoConteudo(rs.getInt("codigoConteudo"));
				conteudoMateria.add(c);
				}
			
			}
			catch(SQLException e)
				{
				imprimeErro("Erro ao listar conteudo",
				e.getMessage());
			
				}
			finally
			{
			fechar();
			}
		
		
		return conteudoMateria;
	}
	
	public ArrayList<Conteudo> listaTodosConteudos(int codigo){
		conectar();
		ArrayList<Conteudo> listaConteudo 
		      = new ArrayList<Conteudo>();
		try {
		
		PreparedStatement selecionarConteudo = null;
		ResultSet rs = null;
		
		String sql = "SELECT * FROM conteudo WHERE codigoMateria=" +codigo;
		
		selecionarConteudo = con.prepareStatement(sql);
		rs = selecionarConteudo.executeQuery(sql);
		
		while(rs.next()){
			Conteudo f = new Conteudo();
			f.setNomeConteudo(rs.getString("nomeConteudo"));
	
			listaConteudo.add(f);
		}
		
		} catch(SQLException e){
			imprimeErro("Erro ao selecionar Conteudos", 
					e.getMessage());
		}finally {
		    fechar();
	    }
		
		return listaConteudo;
	}

	public ArrayList<Conteudo> listaPorNomeConteudo(String text, int codigo) {
		// TODO Auto-generated method stub
		conectar();
		ArrayList<Conteudo> listaConteudo
		      = new ArrayList<Conteudo>();
		try {
		
		PreparedStatement selecionarFornecedor = null;
		ResultSet rs = null;
		
		String sql = "SELECT * FROM conteudo WHERE "
				+ " nomeConteudo ='" + text + "' AND codigoMateria="+codigo;

		System.out.println(sql);
		selecionarFornecedor = con.prepareStatement(sql);
		
		rs = selecionarFornecedor.executeQuery(sql);
		
		while(rs.next()){
			Conteudo c = new Conteudo();
			c.setDescricao(rs.getString("descricao"));
			c.setCodigoConteudo(rs.getInt("codigoConteudo"));
			
			listaConteudo.add(c);
		}
		
		} catch(SQLException e){
			imprimeErro("Erro ao selecionar Conteudo", 
					e.getMessage());
		}finally {
		    fechar();
	    }
		
		return listaConteudo;
	}
	
	
	
}