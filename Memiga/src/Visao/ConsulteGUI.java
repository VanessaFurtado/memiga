package Visao;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Dao.DaoMemiga;
import Modelo.Conteudo;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.ImageIcon;
import java.awt.Font;

public class ConsulteGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtConteudo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConsulteGUI frame = new ConsulteGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ConsulteGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(-6, 0, 3000, 735);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNomeDoConteudo = new JLabel("Nome do Conteudo");
		lblNomeDoConteudo.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblNomeDoConteudo.setBounds(236, 121, 181, 18);
		contentPane.add(lblNomeDoConteudo);
		
		txtConteudo = new JTextField();
		txtConteudo.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		txtConteudo.setBounds(496, 118, 238, 23);
		contentPane.add(txtConteudo);
		txtConteudo.setColumns(10);
		
		
		
		JTextArea txtDescricao = new JTextArea();
		txtDescricao.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 17));
		txtDescricao.setBounds(73, 167, 1231, 454);
		txtDescricao.setLineWrap(true);
		contentPane.add(txtDescricao);
		txtDescricao.setOpaque(false);
		
		JButton btnConsultar = new JButton("");
		btnConsultar.setIcon(new ImageIcon(ConsulteGUI.class.getResource("/images/pesquisar.jpg")));
		btnConsultar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			String nome = txtConteudo.getText();
			
			DaoMemiga d = new DaoMemiga();
			ArrayList <Conteudo> conteudo= new ArrayList<Conteudo>();
			
			conteudo = d.listaPorNome(nome);
			
			for(int a=0; a< conteudo.size();a++)
			{
				txtDescricao.setText(conteudo.get(a).descricao);
			}
			
			}
		});
		btnConsultar.setBounds(815, 106, 176, 37);
		contentPane.add(btnConsultar);
		
		JButton button = new JButton("");
		button.setIcon(new ImageIcon(ConsulteGUI.class.getResource("/images/voltar.jpg")));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			dispose();
			}
		});
		button.setBounds(10, 40, 49, 45);
		contentPane.add(button);
		
		JButton btnMenuPrincipal = new JButton("");
		btnMenuPrincipal.setIcon(new ImageIcon(AprendaCGUI.class.getResource("/images/menu.jpg")));
		btnMenuPrincipal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			OpcaoGUI c = new OpcaoGUI();
			c.setVisible(true);
			
			}
		});
		btnMenuPrincipal.setBounds(1114, 51, 238, 56);
		contentPane.add(btnMenuPrincipal);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(OpcaoGUI.class.getResource("/images/fundoMemiga.jpg")));
		lblNewLabel.setBounds(-50, -15, 3000, 725);
		contentPane.add(lblNewLabel);

	}
}
