package Visao;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Dao.DaoMemiga;
import Modelo.Conteudo;
import Modelo.Exercicio;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;

public class ExercicioGUI extends JFrame {

	private JPanel contentPane;
	private JRadioButton rdbtnA;
	private JRadioButton rdbtnB;
	private JRadioButton rdbtnC;
	private JRadioButton rdbtnD;
	private JLabel lblDescricao;
	private static ExercicioGUI b;
	public String resposta;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ExercicioGUI frame = b;
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ExercicioGUI(int codigoConteudo) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(-6, 0, 3000, 735);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		DaoMemiga c = new DaoMemiga();
		ArrayList <Exercicio> exercicio= new ArrayList<Exercicio>();
		
		exercicio = c.listaexercicio(codigoConteudo);

		b=this;
	
		
		JLabel lblExerccio = new JLabel("Exerc\u00EDcio");
		lblExerccio.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblExerccio.setBounds(10, 104, 134, 23);
		contentPane.add(lblExerccio);
		
		rdbtnA = new JRadioButton("");
		buttonGroup.add(rdbtnA);
		rdbtnA.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		rdbtnA.setBounds(10, 320, 204, 23);
		contentPane.add(rdbtnA);
		rdbtnA.setOpaque(false);
		
		rdbtnB = new JRadioButton("");
		buttonGroup.add(rdbtnB);
		rdbtnB.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		rdbtnB.setBounds(10, 407, 204, 23);
		contentPane.add(rdbtnB);
		rdbtnB.setOpaque(false);
		
		
		rdbtnC = new JRadioButton("");
		buttonGroup.add(rdbtnC);
		rdbtnC.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		rdbtnC.setBounds(10, 485, 204, 23);
		contentPane.add(rdbtnC);
		rdbtnC.setOpaque(false);
		
		
		rdbtnD = new JRadioButton("");
		buttonGroup.add(rdbtnD);
		rdbtnD.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		rdbtnD.setBounds(10, 564, 204, 23);
		contentPane.add(rdbtnD);
		rdbtnD.setOpaque(false);
		
		
		JLabel lblFotoExercicio = new JLabel("");
		lblFotoExercicio.setBounds(713, 223, 639, 328);
		contentPane.add(lblFotoExercicio);
		
		JButton btnVerificarResposta = new JButton("");
		btnVerificarResposta.setIcon(new ImageIcon(ExercicioGUI.class.getResource("/images/verificar.jpg")));
		btnVerificarResposta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 if(rdbtnA.isSelected()){
			            JOptionPane.showMessageDialog(null, "Parabens! Certa resposta");
			            OpcaoGUI o = new OpcaoGUI();
			            o.setVisible(true);
			        }
			        else {
			        	JOptionPane.showMessageDialog(null, "N�o foi desta vez! Tente novamente");
			        }
			}
		});
		btnVerificarResposta.setBounds(1179, 563, 176, 55);
		contentPane.add(btnVerificarResposta);
		
		JButton button = new JButton("");
		button.setIcon(new ImageIcon(ExercicioGUI.class.getResource("/images/voltar.jpg")));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			dispose();
			}
		});
		button.setBounds(10, 44, 49, 45);
		contentPane.add(button);
		
		lblDescricao = new JLabel("");
		lblDescricao.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblDescricao.setBounds(10, 138, 1297, 146);
		contentPane.add(lblDescricao);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(OpcaoGUI.class.getResource("/images/fundoMemiga.jpg")));
		lblNewLabel.setBounds(-50, -15, 3000, 725);
		contentPane.add(lblNewLabel);
		
		for(int a=0; a< exercicio.size();a++)
		{
			lblDescricao.setText(exercicio.get(a).Descricao);
			rdbtnA.setText(exercicio.get(a).resposta);
			rdbtnB.setText(exercicio.get(a).respostab);
			rdbtnC.setText(exercicio.get(a).respostac);
			rdbtnD.setText(exercicio.get(a).respostad);
			resposta=exercicio.get(a).resposta;
		}
	}
}
