package Visao;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import Dao.DaoMemiga;
import Modelo.Usuario;
import javax.swing.ImageIcon;

public class MainsGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtEmail;
	private JTextField txtNasc;
	private JTextField txtUsuario;
	private JTextField txtSenhaL;
	private JPasswordField txtSenha;
	private JTextField txtUsuarioC;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainsGUI frame = new MainsGUI();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	public MainsGUI() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(-6, 0, 3000, 735);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setOpaque(true);
				    
		JLabel lblCadastrese = new JLabel("Cadastre-se");
		lblCadastrese.setFont(new Font("Century Schoolbook", Font.BOLD | Font.ITALIC, 30));
		lblCadastrese.setBounds(49, 115, 210, 26);
		contentPane.add(lblCadastrese);
		
		JLabel lblLogin = new JLabel("Login");
		lblLogin.setFont(new Font("Century Schoolbook", Font.BOLD | Font.ITALIC, 30));
		lblLogin.setBounds(1100, 133, 130, 42);
		contentPane.add(lblLogin);
		
		txtNome = new JTextField();
		txtNome.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		txtNome.setBounds(49, 197, 229, 28);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblNome.setBounds(117, 168, 67, 26);
		contentPane.add(lblNome);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblEmail.setBounds(117, 297, 67, 20);
		contentPane.add(lblEmail);
		
		txtEmail = new JTextField();
		txtEmail.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		txtEmail.setBounds(49, 322, 229, 28);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		JLabel lblDataDeNascimento = new JLabel("Data de Nascimento");
		lblDataDeNascimento.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblDataDeNascimento.setBounds(80, 361, 187, 20);
		contentPane.add(lblDataDeNascimento);
		
		txtNasc = new JTextField();
		txtNasc.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		txtNasc.setBounds(49, 386, 229, 28);
		contentPane.add(txtNasc);
		txtNasc.setColumns(10);
		
		JLabel lblSenha = new JLabel("Senha");
		lblSenha.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblSenha.setBounds(124, 425, 60, 20);
		contentPane.add(lblSenha);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblUsuario.setBounds(1110, 249, 92, 20);
		contentPane.add(lblUsuario);
		
		txtUsuario = new JTextField();
		txtUsuario.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		txtUsuario.setBounds(1085, 280, 145, 26);
		contentPane.add(txtUsuario);
		txtUsuario.setColumns(10);
		
		JLabel lblSenha_1 = new JLabel("Senha");
		lblSenha_1.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblSenha_1.setBounds(1120, 361, 60, 20);
		contentPane.add(lblSenha_1);
		
		txtSenhaL = new JPasswordField();
		txtSenhaL.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		txtSenhaL.setBounds(1085, 392, 140, 26);
		contentPane.add(txtSenhaL);
		txtSenhaL.setColumns(10);
		
		txtSenha = new JPasswordField();
		txtSenha.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		txtSenha.setBounds(49, 456, 229, 28);
		contentPane.add(txtSenha);
		
		JButton btnCadastrese = new JButton("");
		btnCadastrese.setIcon(new ImageIcon(MainsGUI.class.getResource("/images/cadMemiga.jpg")));
		btnCadastrese.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				String nome = txtNome.getText();
				String email = txtEmail.getText();
				String senha = txtSenha.getText();
				String usuario = txtUsuarioC.getText();
//				Date data = Date.valueOf(txtNasc.getText());
				
				if (nome.equals("")|| email.equals("")||senha.equals("")|| usuario.equals("")){
					JOptionPane.showMessageDialog(null, "Por favor, preencha todos os campos");
				}else{
				
				Usuario u = new Usuario();
				u.setNome(nome);
				u.setUsuario(usuario);
				u.setEmail(email);
				u.setSenha(senha);
//				u.setNascimento(data);
				
				DaoMemiga m = new DaoMemiga();
				m.insere(u);
				
				OpcaoGUI a = new OpcaoGUI();
				a.setVisible(true);
				}
			}
		});
		btnCadastrese.setBounds(60, 547, 199, 49);
		contentPane.add(btnCadastrese);
		
		JButton btnLogin = new JButton("");
		btnLogin.setIcon(new ImageIcon(MainsGUI.class.getResource("/images/loginMemiga.jpg")));
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				String usuario = txtUsuario.getText();
				String Senha = txtSenhaL.getText();
				
				DaoMemiga d = new DaoMemiga();
				boolean valor = d.validaLogin(usuario, Senha);
				if(valor==false){
					JOptionPane.showMessageDialog(null, "Usu�rio ou senha incorretos");
				}
			}
		});
		btnLogin.setBounds(1085, 536, 164, 51);
		contentPane.add(btnLogin);
		
		JLabel lblUsurio = new JLabel("Usu\u00E1rio");
		lblUsurio.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblUsurio.setBounds(117, 227, 80, 28);
		contentPane.add(lblUsurio);
		
		txtUsuarioC = new JTextField();
		txtUsuarioC.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		txtUsuarioC.setBounds(49, 258, 229, 28);
		contentPane.add(txtUsuarioC);
		txtUsuarioC.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(ConsulteGUI.class.getResource("/images/memiga.jpg")));
		lblNewLabel.setBounds(-50, -15, 3000, 725);
		contentPane.add(lblNewLabel);

	}
}
