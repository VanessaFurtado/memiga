package Visao;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.TextArea;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import Dao.DaoMemiga;
import Modelo.Conteudo;

import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.net.URL;
import java.awt.event.ActionEvent;

public class AprendaCGUI extends JFrame {

	private JPanel contentPane;
	private JTextArea txtAprenda;
	private JLabel teste;
	public int valor;
	private JTextArea txtAprenda_1;
	
    
	
	public static void main(String[] args) {
			EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					AprendaCGUI frame = new AprendaCGUI("",0);
				      frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AprendaCGUI(String nomeConteudo, int materia) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(-6, 0, 3000, 735);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtAprenda_1 = new JTextArea();
		txtAprenda_1.setBounds(53, 118, 1269, 449);
		contentPane.add(txtAprenda_1);	
		txtAprenda_1.setOpaque(false);
		txtAprenda_1.setLineWrap(true);
		txtAprenda_1.setEditable(false);
		
		preencheConteudo(nomeConteudo, materia);
	}
	public void preencheConteudo(String nomeConteudo, int materia){
		
		DaoMemiga c = new DaoMemiga();
		ArrayList <Conteudo> conteudo= new ArrayList<Conteudo>();
		
		conteudo = c.listaPorNomeConteudo(nomeConteudo, materia);
		String con = "";
		for(int a=0; a< conteudo.size();a++)
		{

			con = (con + "\n" +conteudo.get(a).descricao+ "\n");
			valor = (conteudo.get(a).codigoConteudo);
			
			txtAprenda_1.setText(con);
			System.out.println(con);
		}
		
		JButton button = new JButton("");
		button.setIcon(new ImageIcon(AprendaCGUI.class.getResource("/images/voltar.jpg")));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			dispose();
			}
		});
		button.setBounds(10, 39, 49, 45);
		contentPane.add(button);
		
		JButton btnMenuPrincipal = new JButton("");
		btnMenuPrincipal.setIcon(new ImageIcon(AprendaCGUI.class.getResource("/images/menu.jpg")));
		btnMenuPrincipal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			OpcaoGUI c = new OpcaoGUI();
			c.setVisible(true);
			
			}
		});
		btnMenuPrincipal.setBounds(1114, 51, 238, 56);
		contentPane.add(btnMenuPrincipal);
		


				
				JButton btnExercicios = new JButton("");
				btnExercicios.setIcon(new ImageIcon(AprendaCGUI.class.getResource("/images/Exercicios.jpg")));
				btnExercicios.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
					ExercicioGUI c = new ExercicioGUI(valor);
					c.setVisible(true);
					}
				});
				btnExercicios.setBounds(1103, 590, 238, 56);
				contentPane.add(btnExercicios);
				
				JLabel lblNewLabel = new JLabel("");
				lblNewLabel.setIcon(new ImageIcon(OpcaoGUI.class.getResource("/images/fundoMemiga.jpg")));
				lblNewLabel.setBounds(-24, -19, 3000, 725);
				contentPane.add(lblNewLabel);
				

				
	}
}
