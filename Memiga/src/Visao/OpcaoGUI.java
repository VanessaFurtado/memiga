package Visao;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.Image;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class OpcaoGUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OpcaoGUI frame = new OpcaoGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public OpcaoGUI() {
		
//		 ImagePanel panel = new Image(new ImageIcon("images/fundo.png").getImage());
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(-6, 0, 3000, 735);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnAprenda = new JButton("");
		btnAprenda.setIcon(new ImageIcon(OpcaoGUI.class.getResource("/images/aprenda.jpg")));
		btnAprenda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
			AprendaGUI a = new AprendaGUI();
			a.setVisible(true);
			
			}
		});
		btnAprenda.setBounds(172, 240, 224, 94);
		contentPane.add(btnAprenda);
		
		JButton btnConsulte = new JButton("");
		btnConsulte.setIcon(new ImageIcon(OpcaoGUI.class.getResource("/images/consulte.jpg")));
		btnConsulte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			ConsulteGUI c = new ConsulteGUI();
			c.setVisible(true);
			
			}
		});
		btnConsulte.setBounds(869, 240, 224, 94);
		contentPane.add(btnConsulte);
		
		JLabel lblCadastrarConteudo = new JLabel("Cadastrar Conteudo");
		lblCadastrarConteudo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				CadastrarConteudoGUI c = new CadastrarConteudoGUI();
				c.setVisible(true);
			}
		});
		lblCadastrarConteudo.setForeground(Color.WHITE);
		lblCadastrarConteudo.setBounds(1132, 637, 147, 14);
		contentPane.add(lblCadastrarConteudo);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(OpcaoGUI.class.getResource("/images/fundoMemiga.jpg")));
		lblNewLabel.setBounds(10, -15, 3000, 725);
		contentPane.add(lblNewLabel);
	}
}
