package Visao;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;


import Dao.DaoMemiga;
import Modelo.Conteudo;


import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import java.awt.Font;
import javax.swing.JTable;

public class ConteudoGUI extends JFrame {

	private JPanel contentPane;
	private JLabel lblConteudo;
	private JLabel lblNomeConteudo;
	public int codigoConteudo;
	public JTextField txtPesquisa;
	public int codigo;
	private JTable table;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConteudoGUI frame = new ConteudoGUI(0);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ConteudoGUI(int materia) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 3000, 735);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		codigo=materia;

		
		JButton btnMenuPrincipal = new JButton("");
		btnMenuPrincipal.setIcon(new ImageIcon(AprendaCGUI.class.getResource("/images/menu.jpg")));
		btnMenuPrincipal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			OpcaoGUI c = new OpcaoGUI();
			c.setVisible(true);
			
			}
		});
		btnMenuPrincipal.setBounds(1114, 51, 238, 56);
		contentPane.add(btnMenuPrincipal);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(297, 118, 569, 517);
		contentPane.add(scrollPane);
		
		String header[] = {"Nome Conteudo"};
		String data[][] = {{"","","",""}};

		table = new JTable(data,header);
		scrollPane.setViewportView(table);
				
		preencheTabela();

		DaoMemiga dao = new DaoMemiga();
			ArrayList<Conteudo> lista = new ArrayList<Conteudo>();
			lista = dao.listaTodosConteudos(materia);
			DefaultTableModel m = new DefaultTableModel();
			String header2[] = {"Nome Conteudo"};
			
			m.setColumnIdentifiers(header2);

			Object[] rowData = new Object[4];
			for(Conteudo c : lista){
				rowData[0] = c.getNomeConteudo();					
				m.addRow(rowData);
			}
			table.setModel(m);
		
//		txtPesquisa = new JTextField();
//		txtPesquisa.addKeyListener(new KeyAdapter() {
//			@Override
//			public void keyReleased(KeyEvent e) {
//				
//				
//				DaoMemiga dao = new DaoMemiga();
//     			ArrayList<Conteudo> lista = new ArrayList<Conteudo>();
//
//				if(txtPesquisa.getText().isEmpty())
//				{
//					lista = dao.listaTodosConteudos(materia);
//				}else {
//                    lista = dao.listaPorNomeConteudo(txtPesquisa.getText(), materia);
//				}
//
//				DefaultTableModel m = new DefaultTableModel();
//				String header[] = {"Nome Conteudo"};
//				
//				m.setColumnIdentifiers(header);
//
//				Object[] rowData = new Object[4];
//				for(Conteudo c : lista){
//					rowData[0] = c.getNomeConteudo();					
//					m.addRow(rowData);
//				}
//				table.setModel(m);
//			}
//		});
//		txtPesquisa.setBounds(340, 79, 248, 28);
//		contentPane.add(txtPesquisa);
//		txtPesquisa.setColumns(10);
		
		JButton btnVoltar = new JButton("");
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			dispose();
			}
		});
		btnVoltar.setIcon(new ImageIcon(ConteudoGUI.class.getResource("/images/voltar.jpg")));
		btnVoltar.setBounds(10, 40, 49, 45);
		contentPane.add(btnVoltar);

//		JButton btnAtualizar = new JButton("");
//		btnAtualizar.setIcon(new ImageIcon(ConteudoGUI.class.getResource("/images/atualizar.jpg")));
//		btnAtualizar.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				DaoMemiga dao = new DaoMemiga();
//     			ArrayList<Conteudo> lista = new ArrayList<Conteudo>();
//
//                    lista = dao.listaPorNomeConteudo(txtPesquisa.getText(), materia);
//
//				DefaultTableModel m = new DefaultTableModel();
//				String header[] = {"Nome Conteudo"};
//				
//				
//				m.setColumnIdentifiers(header);
//
//				Object[] rowData = new Object[4];
//				for(Conteudo c : lista){
//					rowData[0] = c.getNomeConteudo();
//					
//					m.addRow(rowData);
//				}
//				table.setModel(m);
//			}
//		});
//		btnAtualizar.setBounds(658, 69, 189, 38);
//		contentPane.add(btnAtualizar);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(OpcaoGUI.class.getResource("/images/fundoMemiga.jpg")));
		lblNewLabel.setBounds(-47, -40, 3000, 725);
		contentPane.add(lblNewLabel);
		
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
            public void valueChanged(ListSelectionEvent event) {
             
            	DaoMemiga d = new DaoMemiga();
            	AprendaCGUI a = new AprendaCGUI(String.valueOf(table.getValueAt(table.getSelectedRow(),0)), materia);
            	a.setVisible(true);
            }
        });

	}
	private void preencheTabela() {
		DaoMemiga dao = new DaoMemiga();
			ArrayList<Conteudo> lista = new ArrayList<Conteudo>();

		DefaultTableModel m = new DefaultTableModel();
		String header[] = {"Nome Conteudo"};
		
		m.setColumnIdentifiers(header);

		Object[] rowData = new Object[4];
		for(Conteudo c : lista){
			rowData[0] = c.getNomeConteudo();

			System.out.println(c.getNomeConteudo());
			m.addRow(rowData);
		}
	}
}

