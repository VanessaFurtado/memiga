package Visao;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class AprendaGUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AprendaGUI frame = new AprendaGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AprendaGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(-6, 0, 3000, 735);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnMatematica = new JButton("");
		btnMatematica.setIcon(new ImageIcon(AprendaGUI.class.getResource("/images/Matematica.jpg")));
		btnMatematica.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
			ConteudoGUI a = new ConteudoGUI(1);
			a.setVisible(true);
			
			
			}
		});
		btnMatematica.setBounds(204, 124, 261, 121);
		contentPane.add(btnMatematica);
		
		JButton btnQuimica = new JButton("");
		btnQuimica.setIcon(new ImageIcon(AprendaGUI.class.getResource("/images/Qu\u00EDmica.jpg")));
		btnQuimica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ConteudoGUI a = new ConteudoGUI(2);
				a.setVisible(true);
			
			}
		});
		btnQuimica.setBounds(204, 346, 261, 121);
		contentPane.add(btnQuimica);
		
		JButton btnFsica = new JButton("");
		btnFsica.setIcon(new ImageIcon(AprendaGUI.class.getResource("/images/F\u00EDsica.jpg")));
		btnFsica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ConteudoGUI a = new ConteudoGUI(3);
				a.setVisible(true);
				
			
			}
		});
		btnFsica.setBounds(802, 124, 261, 121);
		contentPane.add(btnFsica);
		
		JButton btnIngls = new JButton("");
		btnIngls.setIcon(new ImageIcon(AprendaGUI.class.getResource("/images/Ingles.jpg")));
		btnIngls.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ConteudoGUI a = new ConteudoGUI(4);
				a.setVisible(true);
				
			}
		});
		btnIngls.setBounds(802, 346, 261, 121);
		contentPane.add(btnIngls);
		
		JButton btnVoltar = new JButton("");
		btnVoltar.setIcon(new ImageIcon(AprendaGUI.class.getResource("/images/voltar.jpg")));
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			dispose();
			
			}
		});
		btnVoltar.setBounds(10, 44, 49, 45);
		contentPane.add(btnVoltar);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(OpcaoGUI.class.getResource("/images/fundoMemiga.jpg")));
		lblNewLabel.setBounds(-50, -15, 3000, 725);
		contentPane.add(lblNewLabel);
	}
}
