package Visao;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Dao.DaoMemiga;
import Modelo.Conteudo;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class CadastrarConteudoGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtMateria;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastrarConteudoGUI frame = new CadastrarConteudoGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CadastrarConteudoGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(-6, 0, 3000, 735);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNomeDoConteudo = new JLabel("Nome do Conte�do");
		lblNomeDoConteudo.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblNomeDoConteudo.setBounds(10, 106, 182, 28);
		contentPane.add(lblNomeDoConteudo);
		
		txtNome = new JTextField();
		txtNome.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		txtNome.setBounds(10, 141, 521, 30);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblDescricao = new JLabel("Descri��o");
		lblDescricao.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblDescricao.setBounds(10, 182, 105, 30);
		contentPane.add(lblDescricao);
		
		JTextArea txtConteudo = new JTextArea();
		txtConteudo.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		txtConteudo.setBounds(10, 208, 1342, 296);
		contentPane.add(txtConteudo);
		
		JButton btnCadastrarFilme = new JButton("");
		btnCadastrarFilme.setIcon(new ImageIcon(CadastrarConteudoGUI.class.getResource("/images/conteud.jpg")));
		btnCadastrarFilme.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			String nome = txtNome.getText();
			String descricao = txtConteudo.getText();
			int materia = Integer.parseInt(txtMateria.getText());
			
			Conteudo c = new Conteudo();
			c.setNomeConteudo(nome);
			c.setDescricao(descricao);
			c.setCodigoMateria(materia);
			
			DaoMemiga d = new DaoMemiga();
			d.insereConteudo(c);
			
			JOptionPane.showMessageDialog(null, "Gravado");	
			}
		});
		btnCadastrarFilme.setBounds(1075, 572, 262, 51);
		contentPane.add(btnCadastrarFilme);
		
		JButton button = new JButton("");
		button.setIcon(new ImageIcon(CadastrarConteudoGUI.class.getResource("/images/voltar.jpg")));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			dispose();
			}
		});
		button.setBounds(10, 46, 49, 45);
		contentPane.add(button);
		
		JLabel lblMateria = new JLabel("Mat�ria");
		lblMateria.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblMateria.setBounds(562, 106, 105, 28);
		contentPane.add(lblMateria);
		
		txtMateria = new JTextField();
		txtMateria.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		txtMateria.setBounds(562, 141, 137, 30);
		contentPane.add(txtMateria);
		txtMateria.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(OpcaoGUI.class.getResource("/images/fundoMemiga.jpg")));
		lblNewLabel.setBounds(-50, -15, 3000, 725);
		contentPane.add(lblNewLabel);
	
	}
}
